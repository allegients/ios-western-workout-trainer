//
//  editWorkout.swift
//  Assignment2
//  Swift class of work out screen for a specific exercise
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.09

import UIKit

class editWorkout: UIViewController {
    
    @IBOutlet weak var exerciseTitle: UILabel!
    @IBOutlet weak var setLabel: UILabel!
    @IBOutlet weak var repLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var setStepper: UIStepper!
    @IBOutlet weak var repStepper: UIStepper!
    @IBOutlet weak var weightStepper: UIStepper!
    @IBOutlet weak var saveRest: UIButton!
    
    var exerciseName: String?
    var category: String?
    var saveRow: Int?
    let db = DataBaseManager()
    var reps = 0
    var weight = 0
    var currentSet = 1
    var maxSets = 1
    
    // Timer variables
    var alertController: UIAlertController?
    var alertTimer: Timer?
    var remainingTime = 0
    var baseMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        exerciseTitle.text = exerciseName!
        
        // Grabbing reps, set and weight of exercise from db
        let repsConvert = db.getReps(exercise: exerciseName!)
        reps = Int(repsConvert[0])!
        let setsConvert = db.getSets(exercise: exerciseName!)
        maxSets = Int(setsConvert[0])!
        let weightConvert = db.getWeight(exercise: exerciseName!)
        weight = Int(weightConvert[0])!
        
        // Setting labels, variables and steppers according to amount of reps sets and weight
        setLabel.text = "You are currently on set " + String(currentSet) + "/" + String(maxSets);
        repLabel.text = String(reps)
        weightLabel.text = String(weight)
        setStepper.value = Double(maxSets)
        repStepper.value = Double(reps)
        weightStepper.value = Double(weight)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Sets stepper
    @IBAction func setChange(_ sender: UIStepper) {
        maxSets = Int(sender.value)
        setLabel.text = "You are currently on set " + String(currentSet) + "/" + String(maxSets);
    }
    
    // Reps stepper
    @IBAction func repsStepper(_ sender: UIStepper) {
        repLabel.text = String(sender.value)
        reps = Int(sender.value)
    }
    
    // Weight stepper
    @IBAction func weightStepper(_ sender: UIStepper) {
        weightLabel.text = String(sender.value)
        weight = Int(sender.value)
    }
    
    // Save log to database
    @IBAction func addLog(_ sender: Any) {
        if (maxSets < currentSet) {     // Checking whether user has completed all sets
            complete()
        }
        else {
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let currentDateString:String = dateFormatter.string(from: date)
            db.addLog(date: currentDateString, exerciseName: exerciseName!, reps: reps, sets: currentSet, weight: weight)
            currentSet = currentSet + 1
            setLabel.text = "You are currently on set " + String(currentSet) + "/" + String(maxSets);
            
            // Checking whether the user has completed all sets
            if (maxSets < currentSet) {
                self.showAlertMsg("Rest Timer", message: "You have successfully completed all sets of this exercise. Please rest up and begin a new exercise from the program. \nTime remaining:", time: 60)
            }
            else {
                // Rest timer
                self.showAlertMsg("Rest Timer", message: "Time remaining:", time: 60)
            }
        }
    }
    
    // Alert for when the user has completed all the exercises
    func complete() {
        let alert = UIAlertController(title: "NOTE:", message:
            "You have successfully completed all sets for this exercise! Please return and start a new exercise", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style:
            UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /* Count down timer for resting
     The following code was inspired from https://gist.github.com/anonymous/aadd8e56f2049062c68458c477c28a34
     */
    func showAlertMsg(_ title: String, message: String, time: Int) {
        
        guard (self.alertController == nil) else {
            print("Alert already displayed")
            return
        }
        
        self.baseMessage = message
        self.remainingTime = time
        
        self.alertController = UIAlertController(title: title, message: self.baseMessage, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Skip resting", style: .cancel) { (action) in
            self.alertController=nil;
            self.alertTimer?.invalidate()
            self.alertTimer=nil
        }
        
        self.alertController!.addAction(cancelAction)
        
        self.alertTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countDown), userInfo: nil, repeats: true)
        
        self.present(self.alertController!, animated: true, completion: nil)
    }
    
    func countDown() {
        
        self.remainingTime -= 1
        if (self.remainingTime < 0) {
            self.alertTimer?.invalidate()
            self.alertTimer = nil
            self.alertController!.dismiss(animated: true, completion: {
                self.alertController = nil
            })
        } else {
            self.alertController!.message = self.alertMessage()
        }
    }
    
    func alertMessage() -> String {
        var message=""
        if let baseMessage=self.baseMessage {
            message=baseMessage+" "
        }
        return(message+"\(self.remainingTime)")
    }   // End of code
    
    // Sending back values to original screen
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let secondScreenViewController = segue.destination as? recordWorkout {
            secondScreenViewController.returnScreen = 1
            secondScreenViewController.chosenCategory = category!
            secondScreenViewController.selectedRow = saveRow!
        }
    }
}
// End of editWorkout.swift
