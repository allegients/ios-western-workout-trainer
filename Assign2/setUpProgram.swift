//
//  setUpProgram.swift
//  Assignment2
//  Swift class of screen to setup daily program
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.06

import UIKit

class setUpProgram: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var dayPicker: UIPickerView!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var exercise1Picker: UIPickerView!
    @IBOutlet weak var exercise2Picker: UIPickerView!
    @IBOutlet weak var exercise3Picker: UIPickerView!
    @IBOutlet weak var exercise4Picker: UIPickerView!
    @IBOutlet weak var exercise5Picker: UIPickerView!
    @IBOutlet weak var exercise6Picker: UIPickerView!
    @IBOutlet weak var exerciseStepper: UIStepper!
    @IBOutlet weak var noExercise: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var exercise2Label: UILabel!
    @IBOutlet weak var exercise3Label: UILabel!
    @IBOutlet weak var exercise4Label: UILabel!
    @IBOutlet weak var exercise5Label: UILabel!
    @IBOutlet weak var exercise6Label: UILabel!
    
    var categoryData = ["Triceps", "Chest", "Shoulder", "Biceps", "Abs", "Back", "Forearm", "Upper_Leg", "Glutes", "Cardio", "Lower_Leg"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    var exerciseList: [String] = [""]
    var chosenCategory:String? = nil;
    var numberExercise = 1
    let db = DataBaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Adding sources to the pickers
        self.categoryPicker.delegate = self
        self.categoryPicker.dataSource = self
        self.dayPicker.delegate = self
        self.dayPicker.dataSource = self
        self.exercise1Picker.dataSource = self
        self.exercise1Picker.delegate = self
        self.exercise2Picker.dataSource = self
        self.exercise2Picker.delegate = self
        self.exercise3Picker.dataSource = self
        self.exercise3Picker.delegate = self
        self.exercise4Picker.dataSource = self
        self.exercise4Picker.delegate = self
        self.exercise5Picker.dataSource = self
        self.exercise5Picker.delegate = self
        self.exercise6Picker.dataSource = self
        self.exercise6Picker.delegate = self
        
        // Setting min/max value of stepper
        exerciseStepper.minimumValue = 1;
        exerciseStepper.maximumValue = 6;
        
        // Adding rounded corners to the submit button
        submitButton.layer.cornerRadius = 10
        submitButton.clipsToBounds = true
        
        // Hiding all extra exercises
        exercise2Label.isHidden = true
        exercise3Label.isHidden = true
        exercise4Label.isHidden = true
        exercise5Label.isHidden = true
        exercise6Label.isHidden = true
        exercise2Picker.isHidden = true
        exercise3Picker.isHidden = true
        exercise4Picker.isHidden = true
        exercise5Picker.isHidden = true
        exercise6Picker.isHidden = true
        
        // To autoload the first row of tables
        chosenCategory = "Triceps";
        changeExercise();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // Pickerview components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Number of picker count depending on which picker
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == categoryPicker {
            return categoryData.count
        } else if pickerView == dayPicker {
            return days.count
        }
        else {
            return exerciseList.count
        }
    }
    
    // Adding values to row of pickers accordingly
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == categoryPicker {
            return categoryData[row]
        }
        else if pickerView == dayPicker {
            return days[row]
        }
        else {
            return exerciseList[row]
        }
    }
    
    
    // Listener to change the chosen category according to user
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == categoryPicker {
            chosenCategory = categoryData[categoryPicker.selectedRow(inComponent: 0)]
            changeExercise()
            
            // Reloading exercise pickers so it will update the new selected category
            exercise1Picker.reloadAllComponents()
            exercise2Picker.reloadAllComponents()
            exercise3Picker.reloadAllComponents()
            exercise4Picker.reloadAllComponents()
            exercise5Picker.reloadAllComponents()
            exercise6Picker.reloadAllComponents()
        }
    }
    
    // Retreiving value from database and changing exercises according to category chosen
    func changeExercise() {
        exerciseList = db.retrieveExercise(category: chosenCategory!)
    }
    
    // Stepper for number of exercises
    @IBAction func changeValue(_ sender: UIStepper) {
        noExercise.text = String(sender.value)
        numberExercise = Int(sender.value)
        
        // Whether to show or hide picker and label according to amount of exercises chosen
        if (numberExercise >= 2) {
            exercise2Label.isHidden = false
            exercise2Picker.isHidden = false
        } else {
            exercise2Label.isHidden = true
            exercise2Picker.isHidden = true
        }
        
        if (numberExercise >= 3) {
            exercise3Label.isHidden = false
            exercise3Picker.isHidden = false
        } else {
            exercise3Label.isHidden = true
            exercise3Picker.isHidden = true
        }
        
        if (numberExercise >= 4) {
            exercise4Label.isHidden = false
            exercise4Picker.isHidden = false
        } else {
            exercise4Label.isHidden = true
            exercise4Picker.isHidden = true
        }
        
        if (numberExercise >= 5) {
            exercise5Label.isHidden = false
            exercise5Picker.isHidden = false
        } else {
            exercise5Label.isHidden = true
            exercise5Picker.isHidden = true
        }
        
        if (numberExercise >= 6) {
            exercise6Label.isHidden = false
            exercise6Picker.isHidden = false
        } else {
            exercise6Label.isHidden = true
            exercise6Picker.isHidden = true
        }
    }
    
    // Adding new daily program to database depending on amount chosen
    @IBAction func addProgram(_ sender: Any) {
        let getDay = days[dayPicker.selectedRow(inComponent: 0)]
        if (numberExercise >= 1) {
            let getExercise = exerciseList[exercise1Picker.selectedRow(inComponent: 0)]
            db.updateExercise(exerciseName: getExercise, day: getDay)
        }
        
        if (numberExercise >= 2) {
            let getExercise = exerciseList[exercise2Picker.selectedRow(inComponent: 0)]
            db.updateExercise(exerciseName: getExercise, day: getDay)
        }
        
        if (numberExercise >= 3) {
            let getExercise = exerciseList[exercise3Picker.selectedRow(inComponent: 0)]
            db.updateExercise(exerciseName: getExercise, day: getDay)
        }
        
        if (numberExercise >= 4) {
            let getExercise = exerciseList[exercise4Picker.selectedRow(inComponent: 0)]
            db.updateExercise(exerciseName: getExercise, day: getDay)
        }
        
        if (numberExercise >= 5) {
            let getExercise = exerciseList[exercise5Picker.selectedRow(inComponent: 0)]
            db.updateExercise(exerciseName: getExercise, day: getDay)
        }
        
        if (numberExercise >= 6) {
            let getExercise = exerciseList[exercise6Picker.selectedRow(inComponent: 0)]
            db.updateExercise(exerciseName: getExercise, day: getDay)
        }
        
        let alert = UIAlertController(title: "NOTE:", message:
            "You've successfully added a new daily program", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style:
            UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
// End of setUpProgram.swift
