//
//  setUpExercise.swift
//  Assignment2
//  Swift class of setup exercise screen
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.05

import UIKit

class setUpExercise: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var repsStepper: UIStepper!
    @IBOutlet weak var setsStepper: UIStepper!
    @IBOutlet weak var weightStepper: UIStepper!
    @IBOutlet weak var repsLabel: UILabel!
    @IBOutlet weak var setsLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    var categoryData = ["Triceps", "Chest", "Shoulder", "Biceps", "Abs", "Back", "Forearm", "Upper_Leg", "Glutes", "Cardio", "Lower_Leg"]
    var reps = 0
    var weight = 0
    var sets = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryPicker.delegate = self
        self.categoryPicker.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // Pickerview content
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return categoryData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categoryData[row]
    }
    
    
    // Reps stepper
    @IBAction func repsChange(_ sender: UIStepper) {
        repsLabel.text = String(sender.value)
        reps = Int(sender.value)
    }
    
    // Reps stepper
    @IBAction func setsChange(_ sender: UIStepper) {
        setsLabel.text = String(sender.value)
        sets = Int(sender.value)
    }
    
    // Weight Stepper
    @IBAction func weightChange(_ sender: UIStepper) {
        weightLabel.text = String(sender.value)
        weight = Int(sender.value)
    }
    
    // Submit function
    @IBAction func submit(_ sender: Any) {
        // If the user hasn't entered an exercise name an error will appear
        if (nameInput.text == "") {
            let alert = UIAlertController(title: "ERROR:", message:
                "Please enter an exercise name", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style:
                UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            // Adding new exercise to db
            let selectedExercise = categoryData[categoryPicker.selectedRow(inComponent: 0)]
            let db = DataBaseManager()
            db.addRow(name: nameInput.text!, category: selectedExercise, reps: reps, sets: sets, weight: weight)
            
            // Alert to let user know the exercise has been added
            let alert = UIAlertController(title: "Success:", message:
                "You have successfully added a new exercise", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style:
                UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            // Resetting the fields back to default
            nameInput.text = "";
            repsStepper.value = Double(1)
            setsStepper.value = Double(1)
            weightStepper.value = Double(1)
            repsLabel.text = String(repsStepper.value)
            setsLabel.text = String(setsStepper.value)
            weightLabel.text = String(weightStepper.value)
        }
    }
}
// End of setUpExercise.swift
