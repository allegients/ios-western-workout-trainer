//
//  recordWorkout.swift
//  Assignment2
//  Swift class of recording workout screen
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.08

import UIKit

class recordWorkout: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var exerciseButton: UIButton!
    
    var returnScreen: Int?
    var currentDateString:String = ""
    var selectedRow = 0
    var chosenCategory:String = ""
    var categoryList: [String] = [""]
    var exerciseList: [String] = [""]
    var exerciseChosen:String = ""
    let db = DataBaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryPicker.delegate = self
        self.categoryPicker.dataSource = self
        
        /* Getting today's day and printing it
         The following code was inspired from https://stackoverflow.com/questions/41068860/get-weekday-from-date-swift-3
         */
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        currentDateString = dateFormatter.string(from: date)  // End of code
        dayLabel.text = "Today is " + currentDateString
        
        // Hiding exercise button to prevent user from starting without selecting an exercise
            exerciseButton.isHidden = true
        
        // Retrieving all exercise category on this day from database
        categoryList = db.retrieveProgram(day: currentDateString)
        
        // Checking whether the user returned from editWorkout screen or not to save previous category chosen
        if (returnScreen == 1) {
            exerciseList = db.retrieveProgramExercise(day: currentDateString, category: chosenCategory)
            self.categoryPicker.selectRow(selectedRow, inComponent: 0, animated: false)
            returnScreen = 0
        }
        else {      // Means user is new to screen
            if (categoryList.count == 0) {        // Checking whether there are programs made for today from db query else it will print an error
                categoryList.append("You have no programs on this day!")
            }
            else {
                // Setting table with data by default since there are programs detected
                chosenCategory = categoryList[0]
                exerciseList = db.retrieveProgramExercise(day: currentDateString, category: chosenCategory)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // Picker view components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return categoryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categoryList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chosenCategory = categoryList[categoryPicker.selectedRow(inComponent: 0)]
        selectedRow = categoryPicker.selectedRow(inComponent: 0)
        exerciseList = db.retrieveProgramExercise(day: currentDateString, category: chosenCategory)
        tableView.reloadData()      // Refresh the table with new data
        exerciseButton.isHidden = true      // Hides start exercise button every time category is chosen till user selects an exercise
    }
    
    
    // Table view components
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        cell.textLabel!.text = exerciseList[indexPath.row]
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        exerciseChosen = exerciseList[indexPath.row]
        exerciseButton.isHidden = false
    }
    
    // Passing exercise name and variables to save current screen data to second screen
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let secondScreenViewController = segue.destination as? editWorkout {
            secondScreenViewController.exerciseName = exerciseChosen
            secondScreenViewController.category = chosenCategory
            secondScreenViewController.saveRow = selectedRow
        }
    }
}
// End of recordWorkout.swift
