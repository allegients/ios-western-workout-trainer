//
//  tabBarController.swift
//  Assignment2
//  Swift class of tab bar controller
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.0

import UIKit

class tabBarController: UITabBarController {
    
    var previousTab:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Returns back to log tab if user was on display logs screen
        if (previousTab == 1) {
            self.selectedIndex = 1;
            previousTab = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
// End of tabBarController.swift
