//
//  displayLogs.swift
//  Assignment2
//  Swift class of screen which displays the logs in table view
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.06

import UIKit
import Social

class displayLogs: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fbButton: UIButton!
    
    var date: String?
    let db = DataBaseManager()
    var logList: [String] = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logList = db.getLog(date: date!)    // Query DB for logs
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // Tableview content
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logList.count
    }
    
    // Setting height of cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        cell.textLabel!.text = logList[indexPath.row]
        cell.textLabel!.numberOfLines = 0;
        cell.textLabel!.lineBreakMode = .byWordWrapping
        return cell;
    }
    
    
    /* Post to facebook
     The following code was modified from https://www.appcoda.com/social-framework-introduction/
     */
    @IBAction func postFB(_ sender: UIButton) {
        if (SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook)) {
            let facebookComposeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookComposeVC?.setInitialText("I just achieved an amazing record today! Thank you iWorkout Trainer for helping me achieve this.")
            self.present(facebookComposeVC!, animated: true, completion: nil)
        }
        else {
            // If the user isn't logged into FaceBook this error will show instead
            let alert = UIAlertController(title: "ERROR:", message:
                "Please log on to FaceBook to share this post. Settings -> FaceBook and log in", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style:
                UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }   // End of code
    
    // Sending variable to return back to log screen than home
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let secondScreenViewController = segue.destination as? tabBarController {
            secondScreenViewController.previousTab = 1
        }
    }
}
// End of displayLogs.swift
