//
//  userLogs.swift
//  Assignment2
//  Swift class of user logs tab
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.0

import UIKit

class userLogs: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let secondScreenViewController = segue.destination as? displayLogs {
            
            // Grabbing chosen date and passing it to another view
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let chosenDate = dateFormatter.string(from: datePicker.date)
            secondScreenViewController.date = chosenDate
        }
    }
}
// End of userLogs.swift
