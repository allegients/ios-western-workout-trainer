//
//  DataBaseManager.swift
//  Assignment2
//  Swift class of database manager
//
//  Created by Timon Leung (18408339)
//  Copyright © 2018 Timon Leung. All rights reserved.
//  Version 1.12

// The following code was modified from Lecture 11 materias on vUWS
import UIKit
import CoreData

class DataBaseManager: NSObject {
    
    // Adding new exercises into the table
    var exercises: [NSManagedObject] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func addRow( name:String, category:String, reps:Int, sets:Int, weight:Int) {
        // set the core data to access the Product Entity
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "ExerciseTable", in: managedContext)
        let exercise = NSManagedObject(entity: entity!, insertInto: managedContext)
        exercise.setValue(name,  forKey: "exerciseName")
        exercise.setValue(category, forKey: "category")
        exercise.setValue(reps,  forKey: "reps")
        exercise.setValue(sets, forKey: "sets")
        exercise.setValue(weight,  forKey: "weight")
        do {
            try managedContext.save()
            exercises.append(exercise)
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    
    // Retreiving list of exercises of a category
    func retrieveExercise(category:String) -> [String] {  // return array of Strings
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "category == %@", category)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        do {
            exercises  = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError{
            print(error)
        }
        var msg : [String] = []
        var count = 0
        for exercise in exercises {
            msg.append((exercise.value(forKeyPath: "exerciseName") as? String)!)
            count += 1
        }
        return msg
    }  // End of code
    
    
    /* Updating days to a table
     The following code was modified from https://stackoverflow.com/questions/38751632/update-core-data-object-swift-3
     */
    func updateExercise(exerciseName:String, day:String){
        let predicate = NSPredicate(format: "exerciseName == %@", exerciseName)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        do {
            let test = try context.fetch(fetchRequest)
            if test.count == 1
            {
                let objectUpdate = test[0]
                objectUpdate.setValue(day, forKey: "day")
                do{
                    try context.save()
                }
                catch
                {
                    print(error)
                }
            }
        }
        catch
        {
            print(error)
        }
    }   // End of code
    
    
    /* Retreiving unique categories of a program (select distinct)
     The following code was modified from https://stackoverflow.com/questions/40191598/swift-3-nsfetchrequest-distinct-results
     */
    func retrieveProgram(day:String) -> [String] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "day == %@", day)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        fetchRequest.propertiesToFetch = ["category"]
        fetchRequest.returnsDistinctResults = true
        fetchRequest.resultType = NSFetchRequestResultType.dictionaryResultType
        var msg : [String] = []
        do {
            let results  = try managedContext.fetch(fetchRequest)
            let resultsDict = results as! [[String: String]]
            for r in resultsDict {
                msg.append(r["category"]!)
            }
        }
        catch let error as NSError{
            print(error)
        }
        return msg
    }   // End of code
    
    
    // Retreiving exercises of a program
    func retrieveProgramExercise(day:String, category:String) -> [String] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "day == %@ AND category == %@", day, category)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        do {
            exercises  = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError{
            print(error)
        }
        var msg : [String] = []
        var count = 0
        for exercise in exercises {
            msg.append((exercise.value(forKeyPath: "exerciseName") as? String)!)
            count += 1
        }
        return msg
    }
    
    
    // Get number of reps for an exercise
    func getReps(exercise:String) -> [String] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "exerciseName == %@", exercise)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        do {
            exercises  = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError{
            print(error)
        }
        var msg : [String] = []
        var count = 0
        for exercise in exercises {
            msg.append(String((exercise.value(forKeyPath: "reps") as? Int)!))
            count += 1
        }
        return msg
    }
    
    
    // Get number of sets for an exercise
    func getSets(exercise:String) -> [String] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "exerciseName == %@", exercise)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        do {
            exercises  = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError{
            print(error)
        }
        var msg : [String] = []
        var count = 0
        for exercise in exercises {
            msg.append(String((exercise.value(forKeyPath: "sets") as? Int)!))
            count += 1
        }
        return msg
    }
    
    
    // Get initial weight for an exercise
    func getWeight(exercise:String) -> [String] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "exerciseName == %@", exercise)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseTable")
        fetchRequest.predicate = predicate
        do {
            exercises  = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError{
            print(error)
        }
        var msg : [String] = []
        var count = 0
        for exercise in exercises {
            msg.append(String((exercise.value(forKeyPath: "weight") as? Int)!))
            count += 1
        }
        return msg
    }
    
    
    // Add new entry to log table
    func addLog( date:String, exerciseName:String, reps:Int, sets:Int, weight:Int) {
        // set the core data to access the Product Entity
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Logs", in: managedContext)
        let exercise = NSManagedObject(entity: entity!, insertInto: managedContext)
        exercise.setValue(date,  forKey: "date")
        exercise.setValue(exerciseName, forKey: "exerciseName")
        exercise.setValue(reps,  forKey: "repsNumber")
        exercise.setValue(sets, forKey: "setNumber")
        exercise.setValue(weight,  forKey: "weightNumber")
        do {
            try managedContext.save()
            exercises.append(exercise)
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    
    // Get logs of the day
    func getLog(date:String) -> [String] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [""]}
        let managedContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "date == %@", date)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Logs")
        fetchRequest.predicate = predicate
        do {
            exercises  = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError{
            print(error)
        }
        var msg : [String] = []
        var count = 0
        for exercise in exercises {
            msg.append("Exercise Name: " + (exercise.value(forKeyPath: "exerciseName") as? String)! + "\n Set Number: " + String((exercise.value(forKeyPath: "setNumber") as? Int)!) + "\n Number of Reps: " + String((exercise.value(forKeyPath: "repsNumber") as? Int)!) + "\n Weight: " + String((exercise.value(forKeyPath: "weightNumber") as? Int)!) + "KG")
            count += 1
        }
        return msg
    }
}
// End of DataBaseManager.swift

